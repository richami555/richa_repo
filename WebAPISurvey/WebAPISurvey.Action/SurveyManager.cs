﻿using System.Collections.Generic;
using WebAPISurvey.DataAccess;
using WebAPISurvey.EntityModel;


namespace WebAPISurvey.Action
{
    public class SurveyManager
    {
        private readonly SurveyDA surveyDA;

        public SurveyManager()
        {
            surveyDA = new SurveyDA();
        }

        /// <summary>
        /// Get survey Detail
        /// </summary>
        /// <param name="traineeRegistrationId"></param>
        /// <returns>TraineeCourseMapping</returns>
        public Survey GetSurvey()
        {
            return surveyDA.GetSurvey();
        }
        /// <summary>
        /// Get survey Detail
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns>TraineeCourseMapping</returns>
        public Survey GetSurveyBySurveyId(long surveyId)
        {
            return surveyDA.GetSurveyBySurveyId(surveyId);
        }
        /// <summary>
        /// Get survey List
        /// </summary>
        /// <returns>Survey</returns>
        public List<Survey> GetSurveys()
        {
            return surveyDA.GetSurveys();
        }
        /// <summary>
        /// Create & update TraineeCourseMapping
        /// </summary>
        /// <param name="traineeCourseMapping"></param>
        /// <returns>TraineeCourseMapping</returns>
        public Survey SaveSurvey(Survey survey)
        {
            if (survey == null)
            {
                return null;
            }
            var surveyResult = new Survey();
            // Chaeck is it Add or Update call
            // IPO Slots can only add not update.
            var surveyExist = surveyDA.GetSurveyBySurveyId(survey.SurveyId);
            if (surveyExist == null)
            {
                surveyResult = surveyDA.AddSurvey(survey);
                return surveyResult;
            }
            else
            {
                surveyResult = surveyDA.UpdateSurvey(survey);
                return surveyResult;
            }
            return surveyResult;
        }

        /// <summary>
        /// Create & update TraineeCourseMapping
        /// </summary>
        /// <param name="traineeCourseMapping"></param>
        /// <returns>TraineeCourseMapping</returns>
        public Survey UpadateSurvey(Survey survey)
        {
            if (survey == null)
            {
                return null;
            }
            var surveyResult = new Survey();
            // Chaeck is it Add or Update call
            // IPO Slots can only add not update.
            var surveyExist = surveyDA.GetSurveyBySurveyId(survey.SurveyId);
            if (surveyExist != null)
            {
                surveyResult = surveyDA.UpdateSurvey(survey);
                return surveyResult;
            }
            return surveyResult;
        }
    }
}
