﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace WebAPISurvey.EntityModel
{
    public class SurveyDBContext : DbContext
    {
        public SurveyDBContext()
            : base("name=SurveyDBContext")
        {
        }
        public SurveyDBContext(string databaseConnectionString)
            : base(databaseConnectionString)
        {

        }

       public virtual DbSet<Survey> Survey { get; set; }
    }
}
