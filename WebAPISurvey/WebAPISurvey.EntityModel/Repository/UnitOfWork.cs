﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace WebAPISurvey.EntityModel.Repository
{
    public class UnitOfWork : IUnitOfWork<DbContext>
    {
        private readonly SurveyDBContext _surveyDBContext;
        private readonly DbTransaction _surveyDbTransaction;
        private bool _useTransaction = true;

        public bool UseTransaction
        {
            get { return _useTransaction; }
            set { _useTransaction = value; }
        }

        #region Constructor(s)

        public UnitOfWork()
        {
            _surveyDBContext = new SurveyDBContext();
        }

        public UnitOfWork(SurveyDBContext SurveyDBContext, DbTransaction asmlDbTransaction, bool useTransaction)
        {
            this._surveyDBContext = SurveyDBContext;
            this._surveyDbTransaction = asmlDbTransaction;
            _useTransaction = useTransaction;
        }

        #endregion

        #region Public Functions/Properties
        public DbContext Context { get { return _surveyDBContext; } }

        public SurveyDBContext BeakerContext
        {
            get { return _surveyDBContext; }
        }

        public DbTransaction SurveyDbTransaction
        {
            get { return _surveyDbTransaction; }
        }

        public void Save()
        {
            try
            {
                UseParentTransaction(_useTransaction);
                _surveyDBContext.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public void UseParentTransaction(bool useParentTransaction)
        {
            if (SurveyDbTransaction != null && useParentTransaction)
            {
                BeakerContext.Database.UseTransaction(SurveyDbTransaction);
            }
        }
        public void ManageEntityValidation(bool validationRequired)
        {
            _surveyDBContext.Configuration.ValidateOnSaveEnabled = validationRequired;
        }
        #endregion

        #region Dispose

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _surveyDBContext.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion Dispose
    }
}
