﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace WebAPISurvey.EntityModel.Repository
{
    internal interface IGenericRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetQuery();

        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>,
                               IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "");

        TEntity GetById(object entityId);

        void Insert(TEntity entity);

        void BulkInsert(List<TEntity> entity);

        void Delete(object entityId);

        void Delete(TEntity entity);

        void Update(TEntity entity);
        bool Exists(Expression<Func<TEntity, bool>> filter);
    }
}
