﻿using System;
using System.Data.Entity;

namespace WebAPISurvey.EntityModel.Repository
{
    public interface IUnitOfWork : IDisposable

    {
        void Save();
        void ManageEntityValidation(bool validationRequired);
    }
    /// <summary>
    /// Generic unit of work interface for different DbContext objects.
    /// </summary>
    /// <remarks>
    /// Keyword "out" is specified for the generic parameter
    /// to allow casting (covariance).
    /// </remarks>
    /// <typeparam name="TContext">Concrete DbContext type.</typeparam>
    public interface IUnitOfWork<out TContext> : IUnitOfWork
       where TContext : DbContext
    {
        TContext Context { get; }
    }
}
