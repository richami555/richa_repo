﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace WebAPISurvey.EntityModel.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity>
where TEntity : class
    {
        private readonly IUnitOfWork<DbContext> _unitOfWork;
        internal readonly DbSet<TEntity> DbSet;

        /// <summary>
        /// Creates a new repository.
        /// </summary>
        /// <param name="unitOfWork">The unit of work to use.</param>
        public GenericRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = (IUnitOfWork<DbContext>)unitOfWork;
            DbSet = _unitOfWork.Context.Set<TEntity>();
        }

        /// <summary>
        /// Get query on current entity
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetQuery()
        {
            return DbSet;
        }

        /// <summary>
        /// Performs read operation on database using db entity
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>,
                                                IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return orderBy != null ? orderBy(query).ToList() : query.ToList();
        }

        /// <summary>
        /// Performs read by id operation on database using db entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TEntity GetById(object id)
        {
            return DbSet.Find(id);
        }


        public virtual bool Exists(Expression<Func<TEntity, bool>> filter)
        {
            return DbSet.Any(filter);
        }

        /// <summary>
        /// Performs add operation on database using db entity
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Insert(TEntity entity)
        {
            DbSet.Add(entity);
        }
        /// <summary>
        /// Performs add operation on database using db entity
        /// </summary>
        /// <param name="entityList">The entity list.</param>
        public virtual void BulkInsert(List<TEntity> entityList)
        {
            DbSet.AddRange(entityList);
        }
        /// <summary>
        /// Performs delete by id operation on database using db entity
        /// </summary>
        /// <param name="id"></param>
        public virtual void Delete(object id)
        {
            TEntity entityToDelete = DbSet.Find(id);
            Delete(entityToDelete);
        }

        /// <summary>
        /// Performs Bulk delete operation on database using db entity
        /// </summary>
        /// <param name="id"></param>
        public virtual void BulkDelete(List<TEntity> entityList)
        {
            DbSet.RemoveRange(entityList);
        }

        /// <summary>
        /// Performs delete operation on database using db entity
        /// </summary>
        /// <param name="entityToDelete"></param>
        public virtual void Delete(TEntity entityToDelete)
        {
            if (_unitOfWork.Context.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);
        }

        /// <summary>
        /// Performs update operation on database using db entity
        /// </summary>
        /// <param name="entityToUpdate"></param>
        public virtual void Update(TEntity entityToUpdate)
        {
            DbSet.Attach(entityToUpdate);
            _unitOfWork.Context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public virtual bool PartialUpdate(TEntity entity, params Expression<Func<TEntity, object>>[] propsToUpdate)
        {
            var isUpdateSucceed = false;
            DbSet.Attach(entity);
            var contextEntry = _unitOfWork.Context.Entry(entity);
            _unitOfWork.Context.Configuration.ValidateOnSaveEnabled = false;
            foreach (var prop in propsToUpdate)
            {
                var errors = contextEntry.Property(prop).GetValidationErrors();
                if (errors.Count == 0)
                {
                    contextEntry.Property(prop).IsModified = true;
                    isUpdateSucceed = true;
                }
                else
                {
                    isUpdateSucceed = false;
                }
            }
            return isUpdateSucceed;
        }
    }
}
