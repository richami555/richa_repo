﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace WebAPISurvey.EntityModel
{
    /// <summary>
    /// Module         :    WebAPISurvey.EntityModel
    /// Author         :    Compunnel
    /// Created Date   :    12/01/2016
    /// Purpose        :    This class is used for Survey table.
    /// </summary>
    [Table("Survey")]
    public class Survey
    {
        /// <summary>
        /// Gets or sets the SurveyId.
        /// </summary>
        /// <value>
        /// The SurveyId.
        /// </value>
        [Key]
        [Required]
        public long SurveyId { get; set; }
        /// <summary>
        /// Gets or sets the SurveyName.
        /// </summary>
        /// <value>
        /// The SurveyName.
        /// </value>
        public string SurveyName { get; set; }
        /// <summary>
        /// Gets or sets the SurveyDescription.
        /// </summary>
        /// <value>
        /// The SurveyDescription.
        /// </value>
        public string SurveyDescription { get; set; }
        /// <summary>
        /// Gets or sets the IsActive.
        /// </summary>
        /// <value>
        /// The IsActive.
        /// </value>
        public bool IsActive { get; set; }
        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [Required]
        public string CreatedBy { get; set; }
        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        /// <value>
        /// The created on.
        /// </value>
        [Required]
        public DateTime CreatedOn { get; set; }
        /// <summary>
        /// Gets or sets the updated by.
        /// </summary>
        /// <value>
        /// The updated by.
        /// </value>
        [Required]
        public string UpdatedBy { get; set; }
        /// <summary>
        /// Gets or sets the updated on.
        /// </summary>
        /// <value>
        /// The updated on.
        /// </value>
        [Required]
        public DateTime UpdatedOn { get; set; }
        /// <summary>
        /// Gets or sets the RowGuid.
        /// </summary>
        /// <value>
        /// The RowGuid.
        /// </value>
        [Required]
        public Guid RowGuid { get; set; }
    }
}
