﻿using System.IO;
using System.Web.Http;
using log4net.Config;
using WebAPISurvey.Network;

namespace WebAPISurvey.Network
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            //log4net settings
            var l4Net = Server.MapPath("~/log4net.config");
            XmlConfigurator.ConfigureAndWatch(new FileInfo(l4Net));
            
        }

       
    }
}
