﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPISurvey.Action;
using WebAPISurvey.EntityModel;
using System.Transactions;
using AutoMapper;


namespace WebAPISurvey.Network
{
    /// <summary>
    /// Module         :     WebAPISurvey.Network
    /// Author         :    Compunnel
    /// Created Date   :    12/01/2016
    /// Purpose        :    This class is used for SurveyController.
    /// </summary>
    public class SurveyController : ApiController
    {
        /// <summary>
        /// Gets the Survey.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetSurvey()
        {
            HttpResponseMessage response;
            try
            {
                var surveyManager = new SurveyManager();
                var survey = surveyManager.GetSurvey();
                response = Request.CreateResponse(HttpStatusCode.OK, survey);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error123");
            }

            return response;
        }
        /// <summary>
        /// Gets the Survey By SurveyId.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetSurveyBySurveyId(long surveyId)
        {
            HttpResponseMessage response;
            try
            {
                var surveyManager = new SurveyManager();
                var survey = surveyManager.GetSurveyBySurveyId(surveyId);
                response = Request.CreateResponse(HttpStatusCode.OK, survey);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error1..");
            }

            return response;
        }
        /// <summary>
        /// Gets the Survey List.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetSurveys()
        {
            HttpResponseMessage response;
            try
            {
                var surveyManager = new SurveyManager();
                var survey = surveyManager.GetSurveys();
                response = Request.CreateResponse(HttpStatusCode.OK, survey);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error2..");
            }
            return response;
        }
        /// <summary>
        /// Create/Update Survey.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage AddUpdateSurvey(Survey surveyRequest)
        {
            HttpResponseMessage response;
            try
            {
                var surveyManager = new SurveyManager();
               
                var createdSurvey = surveyManager.SaveSurvey(surveyRequest);
                if (createdSurvey != null)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, createdSurvey);
                    

                }
                else
                    response = Request.CreateResponse(HttpStatusCode.NotFound);

            }
            catch (Exception ex)
            {

                response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error!!!!");

            }
            return response;
        }

        /// <summary>
        /// Create/Update Survey.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage UpdateSurvey(Survey surveyRequest)
        {
            HttpResponseMessage response;
            try
            {
                var surveyManager = new SurveyManager();

                var createdSurvey = surveyManager.UpadateSurvey(surveyRequest);
                if (createdSurvey != null)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, createdSurvey);


                }
                else
                    response = Request.CreateResponse(HttpStatusCode.NotFound);

            }
            catch (Exception ex)
            {

                response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error!!!!");

            }
            return response;
        }
    }
}