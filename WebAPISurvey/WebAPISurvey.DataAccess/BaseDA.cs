﻿using System;
using System.Data.Common;
using System.Data.Entity;
using WebAPISurvey.EntityModel;
using WebAPISurvey.EntityModel.Repository;
using System.Configuration;
using System.Data.SqlClient;

namespace WebAPISurvey.DataAccess
{
    public class BaseDA : IDisposable
    {
        public IUnitOfWork<DbContext> UnitOfWork { get; set; }

        #region Constructor(s)

        /// <summary>
        /// Constructor for the data access.
        /// </summary>
        protected BaseDA()
        {
            disposed = false;
            UnitOfWork = new UnitOfWork();
        }

        protected BaseDA(IUnitOfWork<DbContext> unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        protected BaseDA(SurveyDBContext dbContext, DbTransaction dbTransaction, bool useTransaction)
        {
            SetUnitOfWork(dbContext, dbTransaction, useTransaction);
        }

        private void SetUnitOfWork(SurveyDBContext dbContext, DbTransaction dbTransaction, bool useTransaction)
        {
            disposed = false;
            if (dbContext == null || dbTransaction == null)
                UnitOfWork = UnitOfWork;
            else
            {
                UnitOfWork = new UnitOfWork(dbContext, dbTransaction, useTransaction);
            }
        }

        #endregion "Constructor(s)"

        #region Stago Repository Instance
        private GenericRepository<Survey> surveyRepository;
        public GenericRepository<Survey> SurveyRepository
        {
            get { return surveyRepository ?? (surveyRepository = new GenericRepository<Survey>(UnitOfWork)); }
        }

        #endregion Repository Instance

        #region Dispose
        private bool disposed;

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!disposed)
                if (disposing)
                {
                    UnitOfWork.Dispose();
                }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
