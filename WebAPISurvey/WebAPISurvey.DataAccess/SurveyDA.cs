﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPISurvey.EntityModel;

namespace WebAPISurvey.DataAccess
{
    public class SurveyDA : BaseDA
    {
        public Survey GetSurvey()
        {
            var surveyQuery = from sr in SurveyRepository.GetQuery()
                              where sr.IsActive == true
                              select sr;
            return surveyQuery.OrderBy(x => x.SurveyId).ToList().FirstOrDefault<Survey>();
        }
        public Survey GetSurveyBySurveyId(long surveyId)
        {
            var surveyQuery = from sr in SurveyRepository.GetQuery()
                              where sr.IsActive == true && sr.SurveyId == surveyId
                              select sr;
            return surveyQuery.OrderBy(x => x.SurveyId).ToList().FirstOrDefault<Survey>();
        }
        /// <summary>
        /// Gets the Survey list.
        /// </summary>
        /// <returns></returns>
        public List<Survey> GetSurveys()
        {
            var surveyQuery = from sr in SurveyRepository.GetQuery()
                              select sr;
            return surveyQuery.OrderBy(x => x.SurveyId).ToList();
        }
        public Survey AddSurvey(Survey survey)
        {
            SurveyRepository.Insert(survey);
            UnitOfWork.Save();
            return survey;
        }

        public Survey UpdateSurvey(Survey survey)
        {
            SurveyRepository.Update(survey);
            UnitOfWork.Save();
            return survey;
        }
    }
}
